import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenerateIdentityComponent } from './generate-identity.component';

describe('GenerateIdentityComponent', () => {
  let component: GenerateIdentityComponent;
  let fixture: ComponentFixture<GenerateIdentityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GenerateIdentityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenerateIdentityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
