import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HeaderComponent} from './header/header.component';
import { CommonlayoutComponent } from './common/commonlayout/commonlayout.component';
import {StudentinfoComponent} from './studentinfo/studentinfo.component';
import { FooterComponent } from './footer/footer.component';
import { GenerateIdentityComponent } from './generate-identity/generate-identity.component';
import { DocumentlistComponent } from './documentlist/documentlist.component';
import { GenerateCertificateComponent } from './generate-certificate/generate-certificate.component';
import { SignupComponent } from './auth/signup/signup.component';
import { SigninComponent } from './auth/signin/signin.component';
const routes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full' },
   {path:'school',component:HeaderComponent},
  {path:'layout',component:CommonlayoutComponent},
  {path:'studentinfo',component:StudentinfoComponent},
  {path:'footer',component:FooterComponent},
  {path:'generateIdentity',component:GenerateIdentityComponent},
  {path:'generateCertificate',component:GenerateCertificateComponent},
  {path:'documentlist',component:DocumentlistComponent},
  {path:'signup',component:SignupComponent},
  {path:'signin',component:SigninComponent}

];

 

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
