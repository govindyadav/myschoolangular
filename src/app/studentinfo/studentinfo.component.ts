import { Component, Input, OnInit } from "@angular/core";
import { MatDialog, MatDialogConfig } from "@angular/material";
import { StudentformComponent } from '../studentform/studentform.component';
import {StudentService} from  '../services/student.service';
import {Student}  from '../model/Student.model';
 
@Component({
  selector: "app-studentinfo",
  templateUrl: "./studentinfo.component.html",
  styleUrls: ["./studentinfo.component.css"]
})
export class StudentinfoComponent implements OnInit {
  //@Input() studentList: Student[];
  @Input() students: Student[] = [];
  constructor(private dialog: MatDialog, private studentService:StudentService) {}

  

  ngOnInit() {
    this.getStudentData();
  }
getStudentData(){
  this.studentService.getStudentData().subscribe(
    (students:Student[])=> {
      students.forEach(element => {
        for(var i = 1; i <= 20000; i++){
          this.students.push(element);
       }
      
    });
     

  },
  (error) => console.log(error)

  )
}

 
  openModal() {
  const dialogConfig=new MatDialogConfig();
   dialogConfig.disableClose = true;
        dialogConfig.autoFocus = true;
        dialogConfig.data={

        }
  const dialogRef = this.dialog.open( StudentformComponent,
             dialogConfig);
   

  }
}
