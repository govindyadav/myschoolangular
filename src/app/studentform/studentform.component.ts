import {Component,OnInit, Inject,Input, Injectable} from '@angular/core';
import {FormGroup,FormControl} from '@angular/forms';
import {MatDialog,MAT_DIALOG_DATA, MatDialogRef,MatFormFieldModule ,MatInputModule } from "@angular/material";
import {StudentService} from  '../services/student.service';
import {Student}  from '../model/Student.model';

@Component({
  selector: 'app-studentform',
  templateUrl: './studentform.component.html',
  styleUrls: ['./studentform.component.css']
})
export class StudentformComponent implements OnInit {
private student:Student;
formdata;
emailid;
  constructor( public dialogRef: MatDialogRef<StudentformComponent>, @Inject(MAT_DIALOG_DATA) public data : any
  , private studentService:StudentService
) { }


  ngOnInit() {
 this.formdata= new FormGroup({
   emailid:new FormControl("govind@uii.com")
 });   
  }
 

 onsave(data:any){
    
   console.log(data.fathername+"Hey ");

   this.studentService.saveStudent(data).subscribe(
  (response)=>console.log("saved successfully"+response),
  (error)=>console.log("error"+error)
   )
 }




  onNoClick(){
    this.dialogRef.close();
  }

}
